let title = document.getElementById("title");
      let articleGrid = document.getElementsByClassName('article__grid')[0];


      let data =
         `{
   "rss": {
     "-version": "2.0",
     "-xml:base": "http://www.nasa.gov/",
     "-xmlns:atom": "http://www.w3.org/2005/Atom",
     "-xmlns:dc": "http://purl.org/dc/elements/1.1/",
     "-xmlns:itunes": "http://www.itunes.com/dtds/podcast-1.0.dtd",
     "-xmlns:media": "http://search.yahoo.com/mrss/",
     "channel": {
       "title": "Earth News",
       "description": "A RSS news feed containing the latest NASA press releases on Earth-observing missions.",
       "link": "http://www.nasa.gov/",
       "atom:link": {
         "-rel": "self",
         "-href": "http://www.nasa.gov/rss/dyn/earth.rss",
         "-self-closing": "true"
       },
       "language": "en-us",
       "managingEditor": "jim.wilson@nasa.gov",
       "webMaster": "brian.dunbar@nasa.gov",
       "docs": "http://blogs.harvard.edu/tech/rss",
       "item": [
         {
           "title": "NASA, NOAA Invite Media to Polar Orbiting Weather Satellite Launch",
           "link": "http://www.nasa.gov/press-release/nasa-noaa-invite-media-to-polar-orbiting-weather-satellite-launch",
           "description": "NASA is accepting media requests for launch coverage of the National Oceanic and Atmospheric Administration (NOAA) Joint Polar Satellite System-2 (JPSS-2) satellite. This is the third satellite in the JPSS series, which will capture data to improve weather forecasts, helping scientists predict and prepare for extreme weather events and climate chan",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/2022-08_jpss2-beauty-hires.png?itok=VmqHcFyU",
             "-length": "2192322",
             "-type": "image/png",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/nasa-noaa-invite-media-to-polar-orbiting-weather-satellite-launch"
           },
           "pubDate": "Thu, 01 Sep 2022 14:08 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "Media Invited to Learn About NASA Tools, Resources for Agriculture",
           "link": "http://www.nasa.gov/press-release/media-invited-to-learn-about-nasa-tools-resources-for-agriculture",
           "description": "NASA will host a media teleconference at 1:30 p.m. CDT Thursday, Aug. 25, to discuss the agency\u2019s tools, resources, and science research available to the agriculture community.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/nasa_fd_ranching_0_0.png?itok=neL-sc-v",
             "-length": "3885643",
             "-type": "image/png",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/media-invited-to-learn-about-nasa-tools-resources-for-agriculture"
           },
           "pubDate": "Thu, 18 Aug 2022 14:51 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "NASA Transfers Landsat 9 Satellite to USGS to Monitor Earth\u2019s Changes",
           "link": "http://www.nasa.gov/press-release/nasa-transfers-landsat-9-satellite-to-usgs-to-monitor-earth-s-changes",
           "description": "NASA transferred ownership and operational control on Thursday of the Landsat 9 satellite to the U.S. Geological Survey (USGS) in a ceremony in Sioux Falls, South Dakota.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/landsat_9_release.jpeg?itok=AZyQW9bC",
             "-length": "122749",
             "-type": "image/jpeg",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/nasa-transfers-landsat-9-satellite-to-usgs-to-monitor-earth-s-changes"
           },
           "pubDate": "Thu, 11 Aug 2022 16:07 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "NASA Administrator Statement on Chinese Space Debris",
           "link": "http://www.nasa.gov/press-release/nasa-administrator-statement-on-chinese-space-debris",
           "description": "NASA Administrator Statement on Chinese Space Debris",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/nasa_meatball_4.jpg?itok=WshAgQPn",
             "-length": "85798",
             "-type": "image/jpeg",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/nasa-administrator-statement-on-chinese-space-debris"
           },
           "pubDate": "Sat, 30 Jul 2022 14:00 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "NASA Administrator Statement on Agency Authorization Bill",
           "link": "http://www.nasa.gov/press-release/nasa-administrator-statement-on-agency-authorization-bill",
           "description": "NASA Administrator Bill Nelson released this statement Thursday following approval by the U.S. Congress for the NASA Authorization Act of 2022, which is part of the Creating Helpful Incentives to Produce Semiconductors (CHIPS) Act of 2022.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/nasa_meatball_1.jpg?itok=GPrBmBv4",
             "-length": "85798",
             "-type": "image/jpeg",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/nasa-administrator-statement-on-agency-authorization-bill"
           },
           "pubDate": "Thu, 28 Jul 2022 15:22 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "NASA Awards Contracts for NOAA GeoXO Spacecraft Phase A Study",
           "link": "http://www.nasa.gov/press-release/goddard/2022/nasa-awards-contracts-for-noaa-geoxo-spacecraft-phase-a-study",
           "description": "On behalf of the National Oceanic and Atmospheric Administration (NOAA), NASA has selected two firms for the Geostationary Extended Observations (GeoXO) Spacecraft Phase A Study. These contracted firms will help meet the objectives of NOAA\u2019s GeoXO Program.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/nasa-logo-web-rgb_0.jpg?itok=mrBnB_c9",
             "-length": "189751",
             "-type": "image/jpeg",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/goddard/2022/nasa-awards-contracts-for-noaa-geoxo-spacecraft-phase-a-study"
           },
           "pubDate": "Tue, 26 Jul 2022 16:00 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "Registration Now Open for NASA 2022 International Space Apps Challenge",
           "link": "http://www.nasa.gov/press-release/registration-now-open-for-nasa-2022-international-space-apps-challenge",
           "description": "The NASA International Space Apps Challenge \u2013 the world\u2019s largest annual hackathon \u2013 returns this year with the theme \u201CMake Space,\u201D which emphasizes NASA\u2019s commitment to inclusivity.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/nasa_space_apps_challenge_registration_image_07142022.png?itok=xGxwpjZT",
             "-length": "1918843",
             "-type": "image/png",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/registration-now-open-for-nasa-2022-international-space-apps-challenge"
           },
           "pubDate": "Fri, 15 Jul 2022 10:10 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "NASA, SpaceX Launch Climate Science Research, More to Space Station",
           "link": "http://www.nasa.gov/press-release/nasa-spacex-launch-climate-science-research-more-to-space-station",
           "description": "A SpaceX Dragon resupply spacecraft carrying more than 5,800 pounds of science experiments, crew supplies, and other cargo is on its way to the International Space Station after launching at 8:44 p.m. EDT Thursday from NASA\u2019s Kennedy Space Center in Florida.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/crs-25-screen-grab_5.jpeg?itok=TaSS961y",
             "-length": "267886",
             "-type": "image/jpeg",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/nasa-spacex-launch-climate-science-research-more-to-space-station"
           },
           "pubDate": "Thu, 14 Jul 2022 20:06 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "NASA Shares Climate, Earth Science Resources at Folklife Festival",
           "link": "http://www.nasa.gov/press-release/nasa-shares-climate-earth-science-resources-at-folklife-festival",
           "description": "Members of the public are invited to explore the many ways space science helps families, communities, and our nation better understand our home planet and become more climate resilient through hands-on and virtual activities during NASA Day at the Smithsonian Folklife Festival Saturday, July 2, on the National Mall in Washington.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/nasa_fd_kickoff4.png?itok=mdQZ4vcV",
             "-length": "3767774",
             "-type": "image/png",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/nasa-shares-climate-earth-science-resources-at-folklife-festival"
           },
           "pubDate": "Wed, 29 Jun 2022 14:28 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         },
         {
           "title": "NASA, ESA Finalize Agreements on Climate, Artemis Cooperation",
           "link": "http://www.nasa.gov/press-release/nasa-esa-finalize-agreements-on-climate-artemis-cooperation",
           "description": "NASA Administrator Bill Nelson and ESA (European Space Agency) Director General Josef Aschbacher signed two agreements Wednesday at the ESA Council meeting in Noordwijk, Netherlands, further advancing the space agencies\u2019 cooperation on Earth science and Artemis missions.",
           "enclosure": {
             "-url": "http://www.nasa.gov/sites/default/files/styles/1x1_cardfeed/public/thumbnails/image/54559.jpg?itok=fhUUCT8J",
             "-length": "3327159",
             "-type": "image/jpeg",
             "-self-closing": "true"
           },
           "guid": {
             "-isPermaLink": "false",
             "#text": "http://www.nasa.gov/press-release/nasa-esa-finalize-agreements-on-climate-artemis-cooperation"
           },
           "pubDate": "Wed, 15 Jun 2022 10:44 EDT",
           "source": {
             "-url": "http://www.nasa.gov/rss/dyn/earth.rss",
             "#text": "Earth News"
           }
         }
       ]
     }
   }
 }`

      let dataObject = JSON.parse(data);
      title.innerText = dataObject.rss.channel.description;
      let article = dataObject.rss.channel.item;

      for (i = 0; i < article.length; i++) {

         articleGrid.innerHTML +=
            `<div class="article__item">
               <div class="article__img"><img src = "${article[i].enclosure['-url']}" title ="${article[i].source['#text']}" alt ="${article[i].source['#text']}"></div>
               <h3 class="article__title">${article[i].title}</h3>
               <div class="article__description">${article[i].description}</div>
               <div class="article__link"></div>
               <div class="article__pub__date"><a href="${article[i].link}">Origion article</a> Publish date: ${article[i].pubDate}</div>
               

      
         </div>`

      }